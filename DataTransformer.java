//package finalassignment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class DataTransformer {
	
	
	ArrayList<ArrayList<String>> rows; // Creating arraylist of ArrayList to store contents in the file

	public boolean clear (  ) {
		rows = new ArrayList<>(); //Initiating the main arraylist again to clear the data stored in the object
		//rows.clear();
		return true;
	}
    //method to read the contents of the file.read and write the contents of the file to a array list until EOF is reached
	public Integer read( String filename ) {
		int totalLines = 0; //defining local variables to store the number rows read and file contents
		String readingFile = null;
		String[] lineContent;
		int columnCount = 0;
		BufferedReader buffer = null;
		try {
			if(filename == null) throw new FileNotFoundException(); 
			buffer = new BufferedReader(new FileReader(filename));
			rows = new ArrayList<>(); //Initializing main array list to hold the child array list data
			while (true) {
				readingFile = buffer.readLine();
				ArrayList<String> row = new ArrayList<String>(); //Initializing child array list data to hold row values
				if (readingFile == null) //Condition to check if the EOF is reached
				{
					break;
				} else {
					
					totalLines++;
					lineContent = readingFile.split("\t"); //Reading the contents in the file by tab space
					for (String temp : lineContent) {
						if (temp.equals("")) {
							row.add("0");
						} else {
							row.add(temp);
						}
					}
					//
					if(totalLines == 1)
					{
						columnCount = row.size();
					}
					else if(row.size() != columnCount )
					{
						for(int iter = row.size() ; iter < columnCount ; iter++)
						{
							row.add("0");
						}
					}
				}
				rows.add(row);
			}
		} catch (FileNotFoundException e) {
			System.out.println("No such file or directory");
			return 0;
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			try {
				if ( buffer != null )
				{
					buffer.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
		}
		return (totalLines - 1);
	}
    //method to add new column to the array list.This method throws exception when null value,empty string is passed as argument
	public boolean newColumn ( String columnName ) {
		try {
			if (columnName == null)
				throw new NullPointerException(); //throws exception when the given column name is null
			if (((rows == null) || (rows.size() == 0 )) && !(columnName.isEmpty())) { //handling the condition when there is no data exist in the main array list and user need to add column still
				rows = new ArrayList<>();
				ArrayList<String> row = new ArrayList<>();
				ArrayList<String> rowValue = new ArrayList<>();
				row.add(columnName);
				rowValue.add("0");
				rows.add(row);
				rows.add(rowValue);
				return true;
			}
			else if (columnName.isEmpty()) { //checking if the given column name is empty
				System.out.println("Empty Column name");
				return false;
			} else {
				int columnExists = getColumnIndex(columnName);
				if (columnExists == -1) {
					for (int i = 0; i < rows.size(); i++) {
						if (i == 0) {
							rows.get(i).add(columnName); //adding column name to the main array list
						} else {
							rows.get(i).add("0"); //assigning data values to the created columns
						}
					}
					return true;
				} else {
					System.out.println(columnName + " already present"); // printing when the column name already exists
					return false;
				}
			}
		} catch (NullPointerException e) {
			System.out.println("System cannot accept null as column value");
			return false;
		}
	}
    //method to calculate and write data to the column specified.This method can only handle regular expression
	public Integer calculate( String equation ) {
		if (equation == null)
		{
			System.out.println("Column name cannot be null");
			return 0;
		}
		equation = equation.trim(); 
		String[] eq = equation.split("\\s+"); // regular expression to handle more spaces between equation		
		Integer[] indexId = new Integer[eq.length];
		
		if(rows != null && rows.size() > 0)
		{
		for (int i = 0; i < eq.length; i++) {
			indexId[i] = getColumnIndex(eq[i]);
		}
		// calculation part
		try {
			if ( eq[1].equalsIgnoreCase("=")) {
			int count=0;
			if ((indexId.length == 3) && (eq[1].equalsIgnoreCase("="))) { //checking for the expression of the format a = b where a and b are column name 
				System.out.println("Index length" + indexId.length);
				ArrayList<String> row = new ArrayList<>();
				if (indexId[2] != -1) { // check for the index value if the index value is not -1 then it is a column
					for (int i = 1; i < rows.size(); i++) {
						row = rows.get(i);
						row.set(indexId[0], row.get(indexId[2]));
						count++;
					} return count;
				} else if (indexId[2] == -1) { // if index value is -1 then it is not a column 
					boolean checkIsNumber = isNumber(eq[2]); //calling method if the given expression is a number
					if (checkIsNumber == true) { //if number then assign the constant value to all the elements in the array list
						for (int i = 1; i < rows.size(); i++) {
							row = rows.get(i);
							Double firstOperand = Double.parseDouble(eq[2]);
							row.set(indexId[0], (int) Math.round(firstOperand) + "");
							count++;
						}
						return count;
					} else { // return the given column name does not exist
						System.out.println("Given column does not exists");
						return 0;
					}
				}

			} else if ((indexId[2] != -1) && (indexId[4] != -1)) { //else if block to handle the expression of the syntax a = b + c where b and c are column name
				ArrayList<String> row = new ArrayList<>();
				for (int i = 1; i < rows.size(); i++) {
					row = rows.get(i);
					Double firstOperand = Double.parseDouble(row.get(indexId[2]));
					Double secondOperand = Double.parseDouble(row.get(indexId[4]));
					String operator = eq[3];
					// calling method to update data to the object based on the
					// operator
					Integer result = calculation(operator, firstOperand, secondOperand);
					row.set(indexId[0], result.toString());
					count++;
				} return count;
			} else if ((indexId[2] == -1) && (indexId[4] != -1)) { //else if block to handle the expression of kind a = b + c where b is number and c is column name
				Double firstOperand = Double.parseDouble(eq[2]);
				ArrayList<String> row = new ArrayList<>();
				for (int i = 1; i < rows.size(); i++) {
					row = rows.get(i);
					Double secondOperand = Double.parseDouble(row.get(indexId[4]));
					String operator = eq[3];
					// calling method to update data to the object based on the
					// operator
					Integer result = calculation(operator, firstOperand, secondOperand);
					row.set(indexId[0], result.toString());
					count++;
				} return count;
			} else if ((indexId[2] != -1) && (indexId[4] == -1)) { //else if block to handle the expression of kind a = b + c where b is column and c is number/constant
				Double secondOperand = Double.parseDouble(eq[4]);
				ArrayList<String> row = new ArrayList<>();
				for (int i = 1; i < rows.size(); i++) {
					row = rows.get(i);
					Double firstOperand = Double.parseDouble(row.get(indexId[2]));
					String operator = eq[3];
					// calling method to update data to the object based on the
					// operator
					Integer result = calculation(operator, firstOperand, secondOperand);
					row.set(indexId[0], result.toString());
					count++;
				} return count;
			} else if ((indexId[2] == -1) && (indexId[4] == -1)) { //else if block to handle when both the arguments are constants
				Double firstOperand = Double.parseDouble(eq[2]);
				Double secondOperand = Double.parseDouble(eq[4]);
				ArrayList<String> row = new ArrayList<>();
				for (int i = 1; i < rows.size(); i++) {
					row = rows.get(i);
					String operator = eq[3];
					// calling method to update data to the object based on the
					// operator
					Integer result = calculation(operator, firstOperand, secondOperand);
					row.set(indexId[0], result.toString());
					count++;
				}
			} return count; //return updated row value
			}
			else
			{
				System.out.println("Given expression is not valid");
			}
		} catch (NumberFormatException e) {
			System.out.println("Given Expression is not valid");
			return 0;
		}
		}
		else
		{
			System.out.println("Given Expression is not valid");
			return 0;
		}
		return 0;
	}
    //method to print the top 5 rows in the array list
	public void top(  ) {
		
		if (rows.size() == 0) { //checking if the size of the main array list is zero
			System.out.println("No data available to print");
		} else {
			ArrayList<String> row = new ArrayList<String>();
			for (int i = 0; i < rows.size() && i<5; i++) { //printing top 5 rows including column name
				row = rows.get(i);
				for (int j = 0; j < row.size(); j++) {
					System.out.print(row.get(j).toString() + "\t");
				}
				System.out.println();

			}
		}

	}
    //method to print all the contents in the main array list 
	public void print(  ) {
		
		try {
			if (rows.size() == 0) { //checking if the size of the main array list is zero
				System.out.println("No data available to print");
			} else {
			for (int i = 0; i < rows.size(); i++) { //printing the available data by checking the main array list size
				ArrayList<String> row = rows.get(i);
				for (int j = 0; j < row.size(); j++) {
					System.out.print(row.get(j).toString() + "\t");
				}
				System.out.println();
			}
		}} catch (Exception e) {
			System.out.println("No data to print"); //handling the exception when no data is available to print(nullpointer exception)
		}

	}
    //method to write the contents of the array list to the file specified in the argument
	public Integer write( String filename ) {
		int count=0;
		try {
			if(rows.size() == 0) throw new NullPointerException(); //throws error when there is no data available to write 
			if((filename == null) || (filename.isEmpty())) throw new FileNotFoundException();
			BufferedWriter writer=new BufferedWriter(new FileWriter(filename));
			for (int i = 0; i < rows.size(); i++) {
				ArrayList<String> row = rows.get(i);
				for (int j = 0; j < row.size(); j++) {
					writer.write(row.get(j).toString() + "\t"); //writing data to a file using write method
				}
				writer.write("\n");
				count++;
				}
			writer.close();
		} catch (NullPointerException e) {
			System.out.println("No data available cannot write empty data to a file");
			return 0;
		} catch (FileNotFoundException e)
		{
			System.out.println("File doesn't exist");
			return 0;
		} catch (IOException e) {
			return 0;
		}
		return count; //returning written count
	}
	//method to get the column index value and to check if the column exist
	private int getColumnIndex(String columnName) {
		ArrayList<String> row = rows.get(0);
		if (row.size() > 0) {
			for (int i = 0; i < row.size(); i++) {
				if (row.get(i).equalsIgnoreCase(columnName))
					return i;
			}
			return -1;
		}
		return -1;
	}
	//method to handle all the operation which is called from the calculate
	private Integer calculation(String operator, Double firstOperand, Double secondOperand) {
		Double result;
		if (operator.equals("*")) {
			result = firstOperand * secondOperand;
			return (int) Math.round(result);
		} else if (operator.equals("/")) {
			if (secondOperand == 0) {
				return (int) Math.round(0);
			} else {
				result = firstOperand / secondOperand;
				return (int) Math.round(result);
			}
		} else if (operator.equals("+")) {
			result = firstOperand + secondOperand;
			return (int) Math.round(result);
		} else if (operator.equals("-")) {
			result = firstOperand - secondOperand;
			return (int) Math.round(result);
		} else {
			System.out.println("Invalid operator");
			return null;
		}
	}
	// to check if the given value is number throws exception if it is not a number
	public boolean isNumber(String value) {
		try {
			Double check = Double.parseDouble(value);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;

	}


}

